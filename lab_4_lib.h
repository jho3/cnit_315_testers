/**
 * lab4_lib.h
 *
 * Provides the function prototypes for lab4_lib.c.
 *
 *		 --------------------------
 * 		|	DO NOT MODIFY THIS FILE. |
 *		 --------------------------
 *
 *      Author: Timothy Becker
 *      Email:	becker43@purdue.edu
 *      Course: CNIT 315
 *      Lab:	4
 *		Created on:	11 March 2015
 */

#ifndef LAB4_LIB_H_
#define LAB4_LIB_H_


int stringLength( char* string, int* length); //- Matt

int vowelCount( char* string, int* count);  //- Omer

int wordCount(char* string, int* count); //- Omer 

int alphaNumeric(char * string, int* check); //- Janeth

int subString(char* string, int startIndex, int endIndex, char* subString);  //- Tim

int concatenate( char* firstString, char* secondString, char* concatString);   //- Jason

int removeWord( char* string, char* removeWord); //- Cody


#endif /* LAB4_LIB_H_ */