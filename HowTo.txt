How to compile, link and run tester:

gcc -c lab_4_lib.c
gcc -c lab_4_tester.c
gcc lab_4_lib.o lab_4_tester.o -o lab_4_tester.out
./lab_4_tester.out 2 > results.txt