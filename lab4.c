/** lab4_test.c: manipulates strings
*
*
* Authors: Group Project
* Course: CNIT 315
* Lab batch:8
* Lab assignment: 4
* Date:3/11/15
*
* Collaborators:
*	Timothy Becker
*	Cody Lambrigger
*	Janeth Vargas
*	Jason Ho
*	Matt DePue
*	Omer Ahmad
*/

#include <stdio.h>
#include <string.h> 

int subStringTest();

int main() {

int failCount = 0;

// Invoke Tims Function: Sub String Test
failCount = failCount + subStringTest();


// Display Final Results
printf("============== Result ==============\n");
if (failCount == 0) {
	printf("\tAll functions Passed!\n");
	printf("\t     Great Job!\n");
}
else
{
	printf("Not all Functions passed the test :(\n\n");
}

return 0;
}

/*
int vowelCount( char* string, int* count);

int wordCount(char* string, int* count);
*/

/*
//Omer
int vowelCountTest() {
	char *testString = "This is a test for Lab number 4 for cnit";
	int *count = NULL;
	int correct = 0;
	vowelCount(testString, count);
if(*count == 10) {
		printf("Vowel Count Test Passed! Good Job\n");
	} else {
		failCount++;
		printf("Vowel Count Test Failed! Please fix the vowelCount\n");
	}
}

//Omer
int wordCountTest() {
	char *testString = "This is a test for Lab number 4 for cnit";
	int *count = NULL;
	int correct = 0;
	wordCount(testString, count);
if(*count == 10) {
		printf("Word Count Test Passed! Good Job\n");
	} else {
		failCount++;
		printf("Word Count Test Failed! Plese fix wordCount\n");
	}
}
*/

//Tim
int subStringTest() {
	printf("\n!!!!!!!!!!!!!!!!! Sub String Test !!!!!!!!!!!!!!!!!\n\n");
	int subStringErrCount = 0;
	char * inputString = malloc(33 * sizeof(char));
	char * subStringOut = malloc(1 * sizeof(char));

	inputString = "I am going to put in this string";
	//printf("Input String: %s\n", inputString);    // For Testing


	//Standard Correctness Check
	if ( subString(inputString, 5 , 11, subStringOut) != 0 ) {
	//if ( 0 != 0 ) {   // For Testing
		printf("Failure - The subString function returned an error on what should have been valid input\n");
		subStringErrCount++;	
	} else {
		//subStringOut = "going t";  // Good Input For Testing
		//subStringOut = "going to"; // bad input For Testing
		if (strcmp(subStringOut, "going t") == 0) {
			printf("Valid Substring Input Check \t- PASS!\n");
		} else {
			printf("Failure - Valid Substring Function Check did NOT Pass. :(\n");
			subStringErrCount++;
		}
	}


	// Second Standard Output Check starting from index 0
	if ( subString(inputString, 0 , 7, subStringOut) != 0 ) {
	//if ( 0 != 0 ) {  // For Testing
		printf("Failure - The subString function returned an error on what should have been valid input\n");
		subStringErrCount++;
	} else {
	
		//subStringOut = "I am goi";   // Good Input For Testing
		//subStringOut = "I am going"; // Bad Input For Testing
		if (strcmp(subStringOut, "I am goi") == 0) {
		printf("Valid Substring Input Check 2 \t- PASS!\n");
		} else {
		subStringErrCount++;
		printf("Failure - Valid Substring Function Check from index = 0 did NOT Pass. :(\n");
		}
	}

	// Checks out of bounds requests
	// To pass, will return something other than 0
	//if ( 1 != 0) {  // For Testing
	if ( subString(inputString, 100 , 105, subStringOut ) != 0) {
		printf("Out of Bounds index check \t- PASS!\n");
	} else {
		printf("Failure - The Substring Function Accepted indexes out of its bounds.\n");
		subStringErrCount++;
	}


	// Checks to catch putting in the indices backwards
	// To pass, will return something other than 0
	if ( subString(inputString, 10 , 5, subStringOut ) != 0) {
	//if ( 1 != 0) {  // For Testing
		printf("Backwards Index Check \t\t- PASS!\n");
	} else {
		printf("Failure - The Substring Function Accepted indexes that were entered backwards\n");
		subStringErrCount++;
	}

if (subStringErrCount == 0) {
	printf("\nSubstring Check Passed :)\n\n");
} else {
	printf("\nSubstring Check Failed :(\n\n");	
}

return subStringErrCount;
}

/*

//Cody
int removeWordTest() {
	char *inputString = "I am going to put in this string";
	char *removeString = "going";
	int removeWord(inputString, removeString);

	if (inputString == "I am  to put in this string") {
		printf("removeWord Function Passed!\n");
	} else {
		failCount++;
		printf("removeWord Function did NOT Pass. :(\n");
	}
}

*/


